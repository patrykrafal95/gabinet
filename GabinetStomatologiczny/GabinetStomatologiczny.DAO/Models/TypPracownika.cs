﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GabinetStomatologiczny.DAO.Models
{
    public enum TypPracownika
    {
        Pacjent,
        Lekarz,
        Administrator
    }
}