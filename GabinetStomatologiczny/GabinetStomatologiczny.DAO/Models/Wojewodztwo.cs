﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GabinetStomatologiczny.DAO.Models
{
    public enum Wojewodztwo
    {
        Lubelskie,
        Opolskie,
        Mazowieckie,
        Podkarpackie,
        Śląskie
    }
}