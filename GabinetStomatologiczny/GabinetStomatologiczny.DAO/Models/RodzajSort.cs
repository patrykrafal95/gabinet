﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GabinetStomatologiczny.DAO.Models
{
    public enum RodzajSort
    {
        name_asc,
        name_desc,
        surname_asc,
        surname_desc,
        pesel_asc,
        pesel_desc,
        plec_asc,
        plec_desc,
        dataurodzenia_asc,
        dataurodzenia_desc,
        nrtel_asc,
        nrtel_desc,
        datarejestracji_asc,
        datarejestracji_desc
    }
}
